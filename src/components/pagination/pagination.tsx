import * as React from 'react';

function Pagination(props: any) {

  const nextPage = () => {
    const newPage = props.page + 1;
    const maxPage = Math.ceil(props.totalEntries / props.perPage);
    if (newPage > maxPage) {
      return;
    }

    props.updateData(props.startDate, newPage, props.perPage);
  }

  const prevPage = () => {
    const page = props.page === 1 ? props.page : props.page - 1;
    props.updateData(props.startDate, page, props.perPage);
  }

  return (
    <div className="pagination">

      <label>
        <span>Rows per page:</span>
        <select name="navRowsPerPage" className="navRowsPerPage" title="">
          <option >20</option>
          <option >50</option>
          <option >100</option>
        </select>
      </label>

      <span className="navFromTo"> {props.perPage * (props.page - 1) + 1} - {props.perPage * props.page > props.totalEntries ? props.totalEntries : props.perPage * props.page} of {props.totalEntries}</span>

      <button onClick={() => prevPage()}>‹</button>

      <button onClick={() => nextPage()}>›</button>

    </div>
  )

}

export default Pagination;
