import * as React from 'react';
import { Route, Redirect } from 'react-router';
import { authenticationService } from '../services/auth.service';

export const PrivateRoute = ({ component: Component, ...rest }: any & { component: React.Component }) => {
  return (
    <Route {...rest} render={props => {
      const currentUser = authenticationService.currentUserValue;
      if (!currentUser) {
        // not logged in so redirect to login page with the return url
        return <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
      }

      // authorised so return component
      return <Component {...props} />
    }} />
  );
}

