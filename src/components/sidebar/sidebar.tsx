import React from 'react';

// CSS
import './sidebar.scss';
// Logo
import logo from '../../sass/img/logo.svg'

function Sidebar() {
  return <nav>
    <a href="/" className="nav-logo">
      <img src={logo} alt="logo" />
    </a>
    <a href="/">
      <span>Home</span>
    </a>
    <a href="/invoices">
      <span>Invoices</span>
    </a>
  </nav>
}

export default Sidebar;
