import React from 'react';

import './header.scss';
import { authenticationService } from '../../services/auth.service';

type HeaderProps = {
  title: string,
}

function Logout() {
  authenticationService.logout();
}

function AppHeader(props: HeaderProps) {
  return (
    <header className="panel-header">
      <h1>{props.title}</h1>
      <p>
        <a href="/login" onClick={() => Logout()}>Logout</a>
      </p>
    </header>
  )
}

export default AppHeader;
