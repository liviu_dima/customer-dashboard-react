import React from 'react';
import Sidebar from '../../components/sidebar/sidebar';
import AppHeader from '../../components/header/header';

class Home extends React.Component {
  render() {
    return <div className="container">
      <aside>
        <Sidebar></Sidebar>
      </aside>
      <main>
        <AppHeader title="Home Customer Dashboard"></AppHeader>
      </main>
    </div>
  }
}

export default Home;
