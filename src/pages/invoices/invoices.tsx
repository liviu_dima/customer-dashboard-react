import * as React from 'react';
import Sidebar from '../../components/sidebar/sidebar';
import AppHeader from '../../components/header/header';
import { getInvoices } from '../../providers/invoices';
import Invoice from './invoice/invoice';
import { IInvoice } from '../../types/invoice';
import './invoices.scss';
import Pagination from '../../components/pagination/pagination';

class Invoices extends React.Component {

  state = {
    results: [],
    page: 1,
    perPage: 20,
    startDate: '2019-05-26',
    totalEntries: 0,
    status: 'loading'
  }

  componentDidMount() {
    this._updateData(
      this.state.startDate,
      this.state.page,
      this.state.perPage,
    );
  }

  _updateData = async (startDate: string, page: number, perPage: number) => {
    const response = await getInvoices(startDate, page, perPage);
    this.setState({
      results: response.results,
      totalEntries: response.count,
      status: 'loaded',
      page: page,
      perPage: perPage,
    });
  }

  render() {
    return (
      <div className="container">
        <aside>
          <Sidebar></Sidebar>
        </aside>
        <main>
          <AppHeader title="Invoices"></AppHeader>
          {this.state.status === 'loading' && <div>Loading...</div>}
          {this.state.status === 'loaded' &&
            <div className="panel">
              <h1 className="panel__header">
                Your Invoices
              <span> ({this.state.totalEntries})</span>
              </h1>
              {
                this.state.results.map((_invoice: IInvoice, index: number) => (
                  <Invoice value={_invoice} key={index}></Invoice>
                ))
              }

              <Pagination
                page={this.state.page}
                perPage={this.state.perPage}
                startDate={this.state.startDate}
                totalEntries={this.state.totalEntries}
                updateData={this._updateData}
              />
            </div>
          }
        </main>
      </div>
    )
  }

}

export default Invoices;
