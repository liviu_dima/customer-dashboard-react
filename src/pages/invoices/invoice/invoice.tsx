import React from 'react';

import '../../../sass/common/order.scss';
import { IInvoice } from '../../../types/invoice';


class Invoice extends React.Component<{ value: IInvoice }> {


  render() {
    return <figure className="order">
      <div className="order-image">
        <img src={this.props.value.filePath ? this.props.value.filePath : ''} alt="No img" />
      </div>

      <figcaption>
        <div className="order-content">
          <h1>
            <span>{this.props.value.deliveryMethod} Shipping</span>
            <span>{this.props.value.name}</span>
          </h1>

          <ul>
            <li>
              <span>Project ID</span>
              {this.props.value.id}
            </li>

            <li>
              <span>Invoice Total</span>
              {this.props.value.total}
            </li>

            <li className="status">
              <span>Status</span>
              {this.props.value.status === 'new' || this.props.value.paymentStatus === 'new' ? 'pending' : 'accepted'}
            </li>

            <li>
              <span>Estimated Delivery</span>
              {this.props.value.deadline}
            </li>
          </ul>
        </div>

        <div className="order-actions">

        </div>
      </figcaption >
    </figure >
  }

}


export default Invoice;
