import React from 'react';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import './login.scss';
import { authenticationService } from '../../services/auth.service';


class Login extends React.Component {

  constructor(props: any) {
    super(props);

    // redirect to home if already logged in
    if (authenticationService.currentUserValue) {
      (this.props as any).history.push('/');
    }
  }

  render() {
    return (
      <div className="login-container">
        <img src={`${process.env.PUBLIC_URL}/img/desktop-collaboration.svg`} alt="" />

        <div className="login-form-container form-container">
          <h1>
            Log in to The/Studio
            <span>Good to see you again!<br />Log in to manage your orders and create more products.</span>
          </h1>

          <Formik
            initialValues={{
              email: '',
              password: ''
            }}
            validationSchema={Yup.object().shape({
              email: Yup.string().required('Email is required'),
              password: Yup.string().required('Password is required')
            })}
            onSubmit={({ email, password }, { setStatus, setSubmitting }) => {
              setStatus();
              authenticationService.login(email, password)
                .then(
                  user => {
                    const { from } = (this.props as any).location.state || { from: { pathname: "/" } };
                    (this.props as any).history.push(from);
                  },
                  error => {
                    setSubmitting(false);
                    setStatus(error);
                  }
                );
            }}
            render={({ errors, status, touched, isSubmitting }) => (
              <Form translate>
                <label>Email Address
                <Field type="email" name="email" placeholder="Email" />
                  <ErrorMessage name="email" component="div" className="invalid-feedback" />
                </label>

                <label>Password
                  <Field type="password" name="password" placeholder="Password" />
                  <ErrorMessage name="password" component="div" className="invalid-feedback" />
                </label>

                <div className="form-actions">
                  <label>
                    <input type="checkbox" id="rememberme" />
                    <span>&nbsp;</span>
                    Keep me signed in
                  </label>
                  {/* <a href="">Forgot Password?</a> */}
                </div>

                <button className="button button-green" type="submit" disabled={isSubmitting}>Login</button>

                {status &&
                  <div className={'alert alert-danger'}>{status}</div>
                }
              </Form>
            )}
          />

          <div className="social-login">
            <button type="button" className="facebook-login">
              <span>
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="white">
                  <path
                    d="M9 8h-3v4h3v12h5v-12h3.642l.358-4h-4v-1.667c0-.955.192-1.333 1.115-1.333h2.885v-5h-3.808c-3.596 0-5.192 1.583-5.192 4.615v3.385z" />
                </svg>
              </span>
              Sign in with Facebook
            </button>
            <button type="button" className="google-login">
              <span>
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 488 512" fill="white">
                  <path
                    d="M488 261.8C488 403.3 391.1 504 248 504 110.8 504 0 393.2 0 256S110.8 8 248 8c66.8 0 123 24.5 166.3 64.9l-67.5 64.9C258.5 52.6 94.3 116.6 94.3 256c0 86.5 69.1 156.6 153.7 156.6 98.2 0 135-70.4 140.8-106.9H248v-85.3h236.1c2.3 12.7 3.9 24.9 3.9 41.4z">
                  </path>
                </svg>
              </span>
              Sign in with Google
            </button>
          </div>


        </div>
      </div>
    )
  }
}

export default Login;
