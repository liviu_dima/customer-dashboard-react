import React from 'react';
import ReactDOM from 'react-dom';
import { Route, BrowserRouter as Router } from 'react-router-dom'
import './index.scss';
import { PrivateRoute } from './components/privateRoute';

// Pages
import Home from './pages/home/home';
import Login from './pages/login/login';
import Invoices from './pages/invoices/invoices';

const routing = (
  <Router>
    <Route path="/login" component={Login} />
    <PrivateRoute exact path="/" component={Home} />
    <PrivateRoute exact path="/invoices" component={Invoices} />
  </Router>
);

ReactDOM.render(routing, document.getElementById('root'));
