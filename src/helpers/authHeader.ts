import { authenticationService } from '../services/auth.service';


export function authHeader() {
  // return authorization header with jwt token
  const currentUser = authenticationService.currentUserValue;
  if (currentUser && currentUser.bearer) {
    return { 'Authorization': `Bearer ${currentUser.bearer}` };
  } else {
    return { 'Authorization': '' };
  }
}
