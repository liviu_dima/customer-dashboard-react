import { authHeader } from '../helpers/authHeader';
import { IInvoice } from '../types/invoice';

export interface IInvoicesResults {
  results: IInvoice[],
  count: number,
}

export async function getInvoices(startDate: string, page: number, perPage: number): Promise<IInvoicesResults> {
  const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
    ,
    ...authHeader()
  };
  return await fetch(`http://localhost:8080/invoices?startDate=${startDate}&page=${page}&perPage=${perPage}`, {
    method: 'GET',
    mode: 'cors',
    headers: headers,
  }).then(response => response.json());
}
